# Generated by Django 2.0.7 on 2018-09-09 20:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Registration', '0009_remove_player_last_clan'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='last_clan',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='last_clan', to='Registration.Clan'),
        ),
    ]
