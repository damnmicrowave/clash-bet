let pp = document.getElementById('potential-profit');
let sum = document.getElementById('id_sum');
let time = document.getElementById('id_time');
let make_a_bet_btn = document.getElementById('make-a-bet');
let create_bet_btn = document.getElementById('create_bet');
let bets_container = document.getElementById('bets');
let bets_container_pc = document.getElementById('bets-pc');
let info = document.getElementById('info');
let connection_div = document.getElementById('connection');
let ws_scheme = window.location.protocol === "https:" ? "wss://" : "ws://";
let accept_bet_id = undefined;
let connection_to_opponent = document.getElementById('syncLongTitle');
let create_bet_range = document.getElementById('id_time');
let balance = document.getElementById('balance');

let is_mobile = false;
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
    is_mobile = true;
}

let operators = document.getElementsByClassName('operator');

let background_gradient_template = "background: -webkit-linear-gradient(-45deg, rgba(0,179,255,0.05) 0%, rgba(0,179,255,0.8) %%, rgba(255,77,0,0.8) %%, rgba(255,77,0,0.05) 100%); background: -o-linear-gradient(-45deg, rgba(0,179,255,0.05) 0%, rgba(0,179,255,0.8) %%, rgba(255,77,0,0.8) %%, rgba(255,77,0,0.05) 100%); background: -ms-linear-gradient(-45deg, rgba(0,179,255,0.05) 0%, rgba(0,179,255,0.8) %%, rgba(255,77,0,0.8) %%, rgba(255,77,0,0.05) 100%); background: linear-gradient(115deg, rgba(0,179,255,0.05) 0%, rgba(0,179,255,0.8) %%, rgba(255,77,0,0.8) %%, rgba(255,77,0,0.05) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00b3ff', endColorstr='#ff4d00', GradientType=1 )";
let emoji_array = ['.&ndash;.', 'ʘ‿ʘ', 'ಠ_ಠ', '(╯°□°）╯︵ ┻━┻', '┬─┬\ufeff ノ( ゜-゜ノ)', '┬─┬⃰͡\u2007(ᵔᵕᵔ͜\u2007)', '┻━┻ ︵ヽ(`Д´)ﾉ︵\ufeff ┻━┻', 'ლ(｀ー´ლ)', 'ʕ•ᴥ•ʔ', 'ʕᵔᴥᵔʔ', 'ʕ •`ᴥ•´ʔ', '(｡◕‿◕｡)', '（\u3000ﾟДﾟ）', '¯\\\\_(ツ)_/¯', '¯\\\\(°_o)/¯', '(`･ω･´)', '(╬ ಠ益ಠ)', '☜(⌒▽⌒)☞', 'ε=ε=ε=┌(;*´Д`)ﾉ', 'ヽ(´▽`)/', 'ヽ(´ー｀)ノ', 'ᵒᴥᵒ#', 'V•ᴥ•V', 'ฅ^•ﻌ•^ฅ', '（ ^_^）o自自o（^_^ ）', 'ಠ‿ಠ', '( ͡° ͜ʖ ͡°)', 'ಥ_ಥ', 'ಥ﹏ಥ', '٩◔̯◔۶', 'ᕙ(⇀‸↼‶)ᕗ', 'ᕦ(ò_óˇ)ᕤ', '⊂(◉‿◉)つ', 'q(❂‿❂)p', '⊙﹏⊙', '¯\\\\_(⊙︿⊙)_/¯', '°‿‿°', '¿ⓧ_ⓧﮌ', '(⊙.☉)7', '(´･_･`)', 'щ（ﾟДﾟщ）', '٩(๏_๏)۶', 'ఠ_ఠ', 'ᕕ( ᐛ )ᕗ', '(⊙_◎)', 'ミ●﹏☉ミ', '༼∵༽ ༼⍨༽ ༼⍢༽ ༼⍤༽', 'ヽ༼ ಠ益ಠ ༽ﾉ', 't(-_-t)', '(ಥ⌣ಥ)', '(づ￣ ³￣)づ', '(づ｡◕‿‿◕｡)づ', '(ノಠ ∩ಠ)ノ彡( \\\\o°o)\\\\', '｡ﾟ( ﾟஇ‸இﾟ)ﾟ｡', '༼ ༎ຶ ෴ ༎ຶ༽', '“ヽ(´▽｀)ノ”', '┌(ㆆ㉨ㆆ)ʃ', '눈_눈', '( ఠൠఠ )ﾉ', '乁( ◔ ౪◔)「      ┑(￣Д ￣)┍', '(๑•́ ₃ •̀๑) ', '⁽⁽ଘ( ˊᵕˋ )ଓ⁾⁾', '◔_◔', '♥‿♥', 'ԅ(≖‿≖ԅ)', '( ˘ ³˘)♥ ', '( ˇ෴ˇ )', 'ヾ(-_- )ゞ', '♪♪ ヽ(ˇ∀ˇ )ゞ', 'ヾ(´〇`)ﾉ♪♪♪', 'ʕ •́؈•̀ ₎', 'ʕ •́؈•̀)', 'ლ(•́•́ლ)', "(ง'̀-'́)ง", '◖ᵔᴥᵔ◗ ♪ ♫ ', '{•̃_•̃}', '(ᵔᴥᵔ)', '(Ծ‸ Ծ)', '(•̀ᴗ•́)و ̑̑', '[¬º-°]¬', '(☞ﾟヮﾟ)☞', "''⌐(ಠ۾ಠ)¬'''", '(っ•́｡•́)♪♬', '(҂◡_◡) ', 'ƪ(ړײ)\u200eƪ\u200b\u200b', '⥀.⥀', 'ح˚௰˚づ ', '♨_♨', '(._.)', '(⊃｡•́‿•̀｡)⊃', '(∩｀-´)⊃━☆ﾟ.*･｡ﾟ', '(っ˘ڡ˘ς)', '( ఠ ͟ʖ ఠ)', '( ͡ಠ ʖ̯ ͡ಠ)', '( ಠ ʖ̯ ಠ)', '(งツ)ว', '(◠﹏◠)', '(ᵟຶ︵ ᵟຶ)', '(っ▀¯▀)つ', 'ʚ(•｀', '(´ж｀ς)', '(° ͜ʖ͡°)╭∩╮', 'ʕʘ̅͜ʘ̅ʔ', 'ح(•̀ж•́)ง † ', '-`ღ´-', '(⩾﹏⩽)', 'ヽ( •_)ᕗ', '~(^-^)~', '\\\\(ᵔᵕᵔ)/'];

class Bet {
    constructor(id, username, cost, time, time_left, ongoing, opponent) {
        this.id = id;
        this.username = username;
        this.cost = cost;
        this.life_time = time;
        this.time_left = time_left;
        this.ongoing = ongoing;
        this.opponent = opponent;
        this.gradient = '';
        this.interval = setInterval(Bet.calc_time_and_gradient.bind(null, this), 1000);
    }

    static calc_time_and_gradient(bet_obj) {
        let minutes_left = Math.floor(bet_obj.time_left / 60).toString();
        let seconds_left = (bet_obj.time_left % 60).toString();
        let timer = document.getElementById('timer-' + bet_obj.id.toString());
        let bet = document.getElementById(bet_obj.id.toString());
        if (seconds_left.length === 1) {
            seconds_left = '0' + seconds_left;
        }
        if (minutes_left.length === 1) {
            minutes_left = '0' + minutes_left;
        }
        if (bet_obj.time_left > 0) {
            bet_obj.gradient = (bet_obj.time_left * 100 / bet_obj.life_time).toFixed(2);
            bet.setAttribute('style', background_gradient_template.replaceAll('%%', bet_obj.gradient.toString() + '%'));
            timer.innerHTML = minutes_left + ':' + seconds_left;
            bet_obj.time_left--;
        } else {
            bet.remove();
            clearInterval(bet_obj.interval);
            bets[bet_obj.id].pop();
        }
    }

}

let bets = {};

for (let i = 0; i < operators.length; i++) {
    operators[i].addEventListener('click', change_sum)
}

let bets_html = (is_mobile) ? document.getElementsByClassName('js-bet-mobile') : document.getElementsByClassName('js-bet-pc');
for (let i=0; i < bets_html.length; i++) {
    let bet_html = bets_html[i];
    get_bet_info(bet_html.id)
}

function set_bet_acception(id) {
    accept_bet_id = id.toString();
}

function start_sync(button) {
    button.classList.add('d-none');
    connection_to_opponent.innerText = 'Соединение...';
    console.log(accept_bet_id);
    betsSocket.send(
        JSON.stringify({
            'bet_id': accept_bet_id,
            'message': 'DUEL_ACCEPTION_REQUEST'
        }));
    button.classList.remove('d-none');
    connection_to_opponent.innerText = 'Вы уверены, что хотите принять эту ставку?';
}

String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

make_a_bet_btn.onclick = function () {
    clear_sum();
    calculate_time();
    count_profit();
};

function change_sum(e) {
    let num = parseInt(e.target.id);
    let curr_sum = parseFloat(sum.value);
    if ((curr_sum + num) > 0) {
        sum.value = (curr_sum + num).toFixed(1);
    }
    clear_sum();
    count_profit();
}

clear_sum();
count_profit();

function clear_sum() {
    sum.value = sum.value.replace(/[^0-9$.]/g, '');
    if (sum.value > parseFloat(balance.innerHTML.replace(',', '.'))) {
        console.log(balance.innerHTML);
        sum.value = balance.innerHTML;
    }
    if (sum.value.split('.').length - 1 > 1) {
        let last_dot_index = null;
        for (let i = 0; i < sum.value.length; i++) {
            if (sum.value[i] === '.') {
                last_dot_index = i;
            }
        }
        sum.value = sum.value.substr(0, last_dot_index) + sum.value.substr(last_dot_index + 1);
    }
    let decimals = retr_dec(sum.value);
    if (decimals > 1) {
        sum.value = sum.value.slice(0, -decimals + 1);
    }
    create_bet_btn.disabled = parseInt(sum.value) < 10 || sum.value === '' || sum.value[0] === '.';
}

function retr_dec(num) {
    return (num.split('.')[1] || []).length;
}

sum.oninput = function () {
    clear_sum();
    count_profit();
};

function count_profit() {
    let comission = null;
    if (info.dataset.noComission === 'true') {
        comission = 1
    } else {
        comission = 0.9
    }
    console.log(comission);
    let profit = (parseFloat(sum.value) * 2 * comission).toFixed(1);
    pp.innerHTML = profit.toString() + " <span class='fas fa-coins'></span>";
}

time.oninput = function () {
    calculate_time();
};

function calculate_time() {
    let minutes = Math.floor(time.value);
    document.getElementById('time').innerHTML = minutes.toString() + word_ending(minutes, ' минут')
}

function word_ending(n, word) {
    if (n % 10 === 1 && n !== 11) {
        return word + 'а'
    } else if ((n % 10 === 2 || n % 10 === 3 || n % 10 === 4)
        && n !== 11 && n !== 12 && n !== 13 && n !== 14) {
        return word + 'ы'
    } else {
        return word
    }
}

function get_bet_info(bet_id) {
    $.ajax({
        url: '/ajax/get_bet_info/' + bet_id + '/',
        dataType: 'json',
        success: function (data) {
            bets[data['id']] = new Bet(data['id'], data['username'], data['cost'], data['time'], data['time_left'], data['ongoing'], data['opponent']);
            update_bets();
        }
    });
}

function update_bets() {
    bets_container.innerHTML = '';
    bets_container_pc.innerHTML = '';
    let bets_template = ``;
    let ids = Object.keys(bets);
    for (let i = 0; i < ids.length; i++) {
        let id = ids[i];
        let bet = bets[id];
        if (is_mobile) {
            if (bet.ongoing) {
                bets_container.innerHTML += `
                <div class="row bet mt-4 mx-2 mx-md-5">
                    <div class="col-12">
                        <div class="row px-2 h-50">
                            <div class="col-6 pt-2 px-0">
                                ${bet.username}
                            </div>
                            <div class="col-6 pt-2 px-0 text-right">
                                ${bet.opponent}
                            </div>
                        </div>
                        <div class="row cost px-2 h-50">
                            <div class="col-6 p-0">${bet.cost}<span class="fas fa-coins ml-1"></span></div>
                            <div class="col-6 p-0 text-right">${bet.cost}<span class="fas fa-coins ml-1"></span></div>
                        </div>
                    </div>
                    <div class="col-12 p-0 text-center" style="position: absolute">
                        <img src="/static/img/swords.gif" height="64" width="64" alt="">
                    </div>
                </div>
                `
            } else {
                bets_container.innerHTML += `
                <div id="${ id }" class="row bet mt-4 mx-2 mx-md-5">
                    <div class="col-6">
                        <div class="row pl-2 pt-2 h-50">
                            ${bet.username}
                        </div>
                        <div class="row cost pl-2 h-50">
                            ${bet.cost}<span class="fas fa-coins ml-1"></span>
                        </div>
                    </div>
                    <div class="timer"><div id="timer-${ id }" class="my-0 text-center">&ndash;&ndash;:&ndash;&ndash;</div>
                    </div>
                    <div class="col-3">
                    </div>
                </div>
                <div class="clicker mx-2" onclick="set_bet_acception(${id})"
                data-toggle="modal" data-target="#sync"></div>
                `
            }
        } else {
            if (i % 2 === 0) {
                bets_template += `<div class="row"><div class="col-6">`;
                if (bet.ongoing) {
                    bets_template += `
                    <div class="row bet js-bet-pc mt-4 mx-2">
                        <div class="col-12">
                            <div class="row px-2 h-50">
                                <div class="col-6 pt-2 px-0">
                                    ${ bet.username }
                                </div>
                                <div class="col-6 pt-2 px-0 text-right">
                                    ${ bet.opponent }
                                </div>
                            </div>
                            <div class="row cost px-2 h-50">
                                <div class="col-6 p-0">${ bet.cost }<span class="fas fa-coins ml-1"></span>
                                </div>
                                <div class="col-6 p-0 text-right">${ bet.cost }<span
                                        class="fas fa-coins ml-1"></span></div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-center" style="position: absolute">
                            <img src="/static/img/swords.gif" height="64" width="64" alt="">
                        </div>
                    </div>`;
                } else {
                    bets_template += `
                    <div id="${ id }" class="row bet js-bet-pc mt-4 mx-2">
                        <div class="col-6">
                            <div class="row pl-2 pt-2 h-50">
                                ${ bet.username }
                            </div>
                            <div class="row cost pl-2 h-50">
                                ${ bet.cost }<span class="fas fa-coins ml-1"></span>
                            </div>
                        </div>
                        <div class="timer">
                            <div id="timer-${ id }" class="my-0 text-center">&ndash;&ndash;:&ndash;&ndash;</div>
                        </div>
                            <div class="col-3">
                        </div>
                    </div>
                    <div class="clicker row mx-2" onclick="set_bet_acception(${id})"
                    data-toggle="modal" data-target="#sync"></div>`;
                }
                bets_template += `</div><div class="col-6">`;
            } else {
                if (bet.ongoing) {
                    bets_template += `
                    <div class="row bet js-bet-pc mt-4 mx-2">
                        <div class="col-12">
                            <div class="row px-2 h-50">
                                <div class="col-6 pt-2 px-0">
                                    ${ bet.username }
                                </div>
                                <div class="col-6 pt-2 px-0 text-right">
                                    ${ bet.opponent }
                                </div>
                            </div>
                            <div class="row cost px-2 h-50">
                                <div class="col-6 p-0">${ bet.cost }<span class="fas fa-coins ml-1"></span>
                                </div>
                                <div class="col-6 p-0 text-right">${ bet.cost }<span
                                        class="fas fa-coins ml-1"></span></div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-center" style="position: absolute">
                            <img src="/static/img/swords.gif" height="64" width="64" alt="">
                        </div>
                    </div>`;
                } else {
                    bets_template += `
                    <div id="${ id }" class="row bet js-bet-pc mt-4 mx-2">
                        <div class="col-6">
                            <div class="row pl-2 pt-2 h-50">
                                ${ bet.username }
                            </div>
                            <div class="row cost pl-2 h-50">
                                ${ bet.cost }<span class="fas fa-coins ml-1"></span>
                            </div>
                        </div>
                        <div class="timer">
                            <div id="timer-${ id }" class="my-0 text-center">&ndash;&ndash;:&ndash;&ndash;</div>
                        </div>
                            <div class="col-3">
                        </div>
                    </div>
                    <div class="clicker row mx-2" onclick="set_bet_acception(${id})"
                    data-toggle="modal" data-target="#sync"></div>`;
                }
                bets_template += `</div></div>`;
            }
        }
    }
    bets_container_pc.innerHTML = bets_template;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function show_connection_status(status) {
    switch (status) {
        case 'connected':
            connection_div.classList.remove('d-none');
            connection_div.innerHTML = 'Подключено к clashbet.net ' + emoji_array[Math.floor(Math.random() * (emoji_array.length))];
            await sleep(3000);
            connection_div.classList.add('d-none');
            connection_div.innerHTML='';
            break;
        case 'reconnecting':
            connection_div.classList.remove('d-none');
            connection_div.innerHTML = 'Соединение с сервером потеряно :/<br>Переподключение...';
            break;
    }
}

create_bet_range.click();

create_bet_range.oninput = function () {
    let min = create_bet_range.getAttribute('min');
    let max = create_bet_range.getAttribute('max');
    let current_value = create_bet_range.value;
    let val = (current_value - min) / (max - min);

    create_bet_range.setAttribute('style',
        'background-image:' +
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ', #00b2ff), '
                + 'color-stop(' + val + ', #ff4d00)'
                + ')'
    );
};

let betsSocket = new ReconnectingWebSocket(ws_scheme + window.location.host + '/ws/bets/');

betsSocket.onopen = async function () {
    await show_connection_status('connected');
};

betsSocket.onclose = async function () {
    await show_connection_status('reconnecting');
};

betsSocket.onmessage = function (e) {
    let data = JSON.parse(e.data)['data'];
    switch (data['message']) {
        case 'BET_CREATED':
            get_bet_info(data['bet_id']);
            break;
        case 'DUEL_ENDED':
            bets[data['bet_id']] = null;
            update_bets();
            break;
        case 'DUEL_READY':
            window.location.replace("http://" + window.location.host + "/duel/" + data['bet_id'] + '/');
            break;
        case 'DUEL_STARTED':
            get_bet_info(data['bet_id']);
            update_bets();
            break;
    }
};