from django.contrib import admin

from Registration.models import Player, Clan, LogBet

admin.site.register(Player)
admin.site.register(Clan)
admin.site.register(LogBet)
