# Generated by Django 2.0.7 on 2018-09-12 10:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Registration', '0012_auto_20180912_0731'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='last_win',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='player',
            name='max_win',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='player',
            name='profit',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='player',
            name='win_streak',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='player',
            name='wins',
            field=models.IntegerField(default=0),
        ),
    ]
