import re
from requests import request as royaleapi_request

from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.forms import forms
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.contrib.auth import views, login as log_in, update_session_auth_hash

from ClashBet import settings
from Registration.forms import SignUpForm
from .models import Player, Clan


def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        errors = []

        username = request.POST.get('username', None)
        password = request.POST.get('password1', None)
        password2 = request.POST.get('password2', None)
        email = request.POST.get('email', None)
        hashtag = request.POST.get('hashtag', None)

        if username == '':
            form.add_error('username', forms.ValidationError('Введите имя пользователя!'))
            errors.append('Введите имя пользователя!')
        if email == '':
            form.add_error('email', forms.ValidationError('Введите почтовый ящик!'))
            errors.append('Введите почтовый ящик!')
        if hashtag is None or hashtag == '':
            form.add_error('hashtag', forms.ValidationError('Введите CR hashtag!'))
            errors.append('Введите CR hashtag!')
        if password == '':
            form.add_error('password1', forms.ValidationError('Введите пароль!'))
            errors.append('Введите пароль!')

        if len(errors) == 0:

            hashtag = hashtag.upper()

            checked_username = re.findall(r'[a-zA-Z_0-9]+', username)
            checked_password = re.findall(r'[a-zA-Z_0-9]+', password)
            checked_hashtag = re.findall(r'#[QLCJPGRYUV0-9]+', hashtag)

            if hashtag not in checked_hashtag:
                form.add_error('hashtag', forms.ValidationError('Введён некорректный CR hashtag!'))
                errors.append('Введён некорректный CR hashtag!')
            if password not in checked_password:
                form.add_error('password1', forms.ValidationError('В пароле используются запрещённые символы!'))
                errors.append('В пароле используются запрещённые символы!')
            elif len(password) > 32:
                form.add_error('password1', forms.ValidationError('Пароль слишком длинный!'))
                errors.append('Пароль слишком длинный!')
            elif len(password) <= 7:
                form.add_error('password1', forms.ValidationError('Пароль слишком короткий!'))
                errors.append('Пароль слишком короткий!')
            if password != password2:
                form.add_error('password1', forms.ValidationError('Пароли не совпадают!'))
                errors.append('Пароли не совпадают!')
            if username not in checked_username:
                form.add_error('username',
                               forms.ValidationError('В имени пользователя используются запрещённые символы!'))
                errors.append('В имени пользователя используются запрещённые символы!')
            elif len(username) > 32:
                form.add_error('username', forms.ValidationError('Имя пользователя слишком длинное!'))
                errors.append('Имя пользователя слишком длинное!')
            elif len(username) <= 3:
                form.add_error('username', forms.ValidationError('Имя пользователя слишком короткое!'))
                errors.append('Имя пользователя слишком короткое!')
            try:
                User.objects.get(username=username)
                form.add_error('username', forms.ValidationError('Игрок с таким именем пользователя уже существует!'))
                errors.append('Игрок с таким именем пользователя уже существует!')
            except User.DoesNotExist:
                pass
        if form.is_valid():
            url = "https://api.royaleapi.com/player/" + hashtag[1:]

            headers = {
                'auth': settings.ROYALE_API_TOKEN
            }

            response = royaleapi_request("GET", url, headers=headers)

            data = response.json()

            if 'error' in data.keys():
                form.add_error('hashtag', forms.ValidationError('Вы ввели недействительный CR hashtag!'))
                errors.append('Вы ввели недействительный CR hashtag!')
            else:
                user = form.save()
                Player.objects.create(
                    user=user, hashtag=hashtag[1:], CRnickname=data['name'])
                log_in(request, user)
        else:
            return render(request, 'registration/register.html', {'form': form, 'errors': errors})
        return redirect('/')
    else:
        form = SignUpForm()
        return render(request, 'registration/register.html', {'form': form})


def logout(request):
    views.logout(request)
    return redirect('/')


@login_required
def change_email(request):
    if request.method == 'POST':
        user = User.objects.get(id=request.user.id)
        user.email = request.POST.get('new_email', None)
        user.save()
        return redirect('/' + user.username + '/profile')
    else:
        return render(request, 'registration/change_email.html')


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('/' + user.username + '/profile')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {
        'form': form
    })
