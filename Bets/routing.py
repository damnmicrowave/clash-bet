from django.conf.urls import url

from .consumers import BetsConsumer

websocket_urlpatterns = [
    url(r'^ws/bets/$', BetsConsumer),
]
