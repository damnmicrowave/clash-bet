from django.conf.urls import url

from Offers.views import daily_offer, daily_offer_take_part, coupons, buy_coupon

app_name = "Offers"

urlpatterns = [
    url(r'^daily_offer/$', daily_offer, name='daily_offer'),
    url(r'^daily_offer/take_part$', daily_offer_take_part, name='daily_offer_take_part'),
    url(r'^coupons/$', coupons, name='coupons'),
    url(r'^coupons/buy/(?P<days>\d+)$', buy_coupon, name='buy_coupon'),
]
