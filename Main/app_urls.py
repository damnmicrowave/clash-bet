from django.conf.urls import url

from Main.views import startpage, faq, tos

app_name = "Main"

urlpatterns = [
    url(r'^startpage/$', startpage, name='start_page'),
    url(r'^faq/$', faq, name='faq'),
    url(r'^tos/$', tos, name='tos'),
]
