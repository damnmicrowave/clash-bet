from django.contrib import admin

from Bets.models import Bet, ExpiredBet, Appeal, BiggestWin

admin.site.register(Bet)
admin.site.register(ExpiredBet)
admin.site.register(Appeal)
admin.site.register(BiggestWin)
