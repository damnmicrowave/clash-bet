from django.db import models

from Registration.models import Player


class Feature(models.Model):
    icon = models.CharField(max_length=20)
    header = models.CharField(max_length=20)
    text = models.TextField(max_length=2000)

    def __str__(self):
        return self.header


class Notification(models.Model):
    player = models.ForeignKey(Player, default=1, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, default='')
    text = models.TextField(max_length=1000, default='')

    def __str__(self):
        return self.text
