let waiting_text = document.getElementsByClassName('waiting-text')[0];
let dots_count = 0;
let bet = document.getElementsByClassName('bet-info')[0];
let timer = document.getElementById('timer');
let bet_id = bet.dataset.betId;
let connection_div = document.getElementById('connection');
let ws_scheme = window.location.protocol === "https:" ? "wss://" : "ws://";

function dots () {
    if (dots_count > 2) {
        waiting_text.innerHTML = 'Ищем противника';
        waiting_text.removeAttribute('style');
        dots_count = 0;
    }
    dots_count++;
    waiting_text.innerHTML += '.';
    waiting_text.setAttribute('style', 'padding-left: ' + (7 * dots_count).toString() + 'px');
}

let search = setInterval(dots, 1000);

function calc_timer() {
    let time_left = parseInt(bet.dataset.timeLeft);
    let minutes_left = Math.floor(time_left / 60).toString();
    let seconds_left = (time_left % 60).toString();
    if (seconds_left.length === 1) {
        seconds_left = '0' + seconds_left;
    }
    if (minutes_left.length === 1) {
        minutes_left = '0' + minutes_left;
    }
    if (time_left > 0) {
        timer.innerHTML = minutes_left + ':' + seconds_left;
        bet.setAttribute('data-time-left', (time_left - 1).toString())
    } else {
        clearInterval(search);
        clearInterval(timer_interval);
        waiting_text.innerHTML = '';
        document.getElementsByClassName('searching')[0].setAttribute('style', 'display: none');
        bet.setAttribute('style', 'border-color: #ff4d00');
        bet.innerHTML = 'Время действия ставки завершилось<br><a href="/">Вернуться на главную</a>';
    }
}

let timer_interval = setInterval(calc_timer, 1000);

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function show_connection_status(status) {
    switch (status) {
        case 'connected':
            connection_div.classList.remove('d-none');
            connection_div.innerHTML = 'Подключено к clashbet.net .&ndash;.';
            await sleep(3000);
            connection_div.classList.add('d-none');
            break;
        case 'reconnecting':
            connection_div.classList.remove('d-none');
            connection_div.innerHTML = 'Соединение с сервером потеряно :/<br>Переподключение...';
            break;
    }
}

let betsSocket = new ReconnectingWebSocket(ws_scheme + window.location.host + '/ws/bets/');

betsSocket.onopen = async function () {
    await show_connection_status('connected');
    betsSocket.send(
        JSON.stringify({
            'bet_id': bet_id,
            'message': 'BET_CREATED'
        }));
    betsSocket.send(
        JSON.stringify({
            'bet_id': bet_id,
            'message': 'DUEL_CREATED'
        }));
};

betsSocket.onclose = async function () {
    await show_connection_status('reconnecting');
};

betsSocket.onmessage = function (e) {
    let data = JSON.parse(e.data)['data'];
    if (data['message'] === 'DUEL_READY') {
        window.location.replace("http://" + window.location.host + "/duel/" + data['bet_id']);
    }
};
