from django.db import models

from Registration.models import Player, Clan


class Bet(models.Model):
    cost = models.FloatField()
    ongoing = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    unfinished = models.BooleanField(default=False)
    expiration = models.DateTimeField()
    acception_timestamp = models.BigIntegerField(default=0)
    expiration_timestamp = models.BigIntegerField(default=0)
    time_left = models.IntegerField(default=0)
    time = models.IntegerField(default=300)
    expiration_ongoing_timestamp = models.BigIntegerField(default=0)
    player = models.ForeignKey(Player, default=1, on_delete=models.CASCADE, related_name='player')
    opponent = models.ForeignKey(Player, default=1, on_delete=models.CASCADE, related_name='opponent')
    winner = models.ForeignKey(Player, default=1, on_delete=models.CASCADE, related_name='winner')
    clan = models.ForeignKey(Clan, default=1, on_delete=models.CASCADE, related_name='clan')

    def __str__(self):
        return str(self.id) + ' ' + self.player.user.username + ' ' + str(self.cost)


class ExpiredBet(models.Model):
    bet_id = models.IntegerField(default=0)
    cost = models.FloatField()
    player = models.ForeignKey(Player, default=1, on_delete=models.CASCADE, related_name='eb_player')
    clan = models.ForeignKey(Clan, default=1, on_delete=models.CASCADE, related_name='eb_clan')
    expiration_timestamp = models.BigIntegerField(default=0)

    def __str__(self):
        return str(self.bet_id) + ' ' + self.player.user.username + ' ' + str(self.cost)


class UploadedFile(models.Model):
    name = models.CharField(max_length=200, default='')
    file = models.FileField(upload_to='media/')

    def __str__(self):
        return self.name


class Appeal(models.Model):
    bet = models.ForeignKey(Bet, default=1, on_delete=models.CASCADE)
    reason = models.CharField(max_length=200, default='')
    text = models.TextField(max_length=5000, default='')
    files = models.ManyToManyField(UploadedFile, default=None)

    def __str__(self):
        return self.reason + ' | ' + self.text[:30] + '...'


class BiggestWin(models.Model):
    is_none = models.BooleanField(default=True)
    player = models.ForeignKey(Player, default=1, on_delete=models.CASCADE)
    win_amount = models.FloatField(default=0.0)
    date = models.DateField()

    def __str__(self):
        return self.player.user.username + ' | ' + str(self.win_amount)
