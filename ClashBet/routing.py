from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import Bets.routing

application = ProtocolTypeRouter({

    'websocket': AuthMiddlewareStack(
        URLRouter(
            Bets.routing.websocket_urlpatterns
        )
    ),
})
