let timer = document.getElementById('timer');

function calc_time() {
    let time_left = parseInt(timer.dataset.timeLeft);
    let hours_left = Math.floor(time_left / 60 / 60).toString();
    let minutes_left = Math.floor((time_left / 60) % 60).toString();
    let seconds_left = (time_left % 60).toString();
    if (seconds_left.length === 1) {
        seconds_left = '0' + seconds_left;
    }
    if (minutes_left.length === 1) {
        minutes_left = '0' + minutes_left;
    }
    if (hours_left.length === 1) {
        hours_left = '0' + hours_left;
    }
    timer.innerHTML = 'Время до завершения: ' + hours_left + ':' + minutes_left + ':' + seconds_left;
    timer.setAttribute('data-time-left', (time_left - 1).toString())
}

setInterval(calc_time, 1000);