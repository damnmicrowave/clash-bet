from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('Main.app_urls')),
    url(r'^', include('Registration.app_urls')),
    url(r'^', include('LK.app_urls')),
    url(r'^', include('Bets.app_urls')),
    url(r'^', include('Offers.app_urls')),
    url(r'^password_reset/$',
        PasswordResetView.as_view(
            template_name='registration/password_reset/password_reset_form.html',
            email_template_name='registration/password_reset/password_reset_email.html',
            subject_template_name='registration/password_reset/password_reset_subject.txt'
        ), name='password_reset'),
    url(r'^password_reset/done/$',
        PasswordResetDoneView.as_view(
            template_name='registration/password_reset/password_reset_done.html'
        ), name='password_reset_done'),
    url(r'^password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirmView.as_view(
            template_name='registration/password_reset/password_reset_confirm.html',
        ), name='password_reset_confirm'),
    url(r'^password_reset/complete/$',
        PasswordResetCompleteView.as_view(
            template_name='registration/password_reset/password_reset_complete.html',
        ), name='password_reset_complete'),
]
