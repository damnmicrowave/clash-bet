let body = document.querySelector('body');
let footer = document.querySelector('footer');
body.setAttribute('style', 'height: ' + (document.body.scrollHeight + footer.clientHeight).toString() + 'px;');

let nav_items = document.getElementsByClassName('nav-link');
for (let i = 0; i < nav_items.length; i++) {
    let item = nav_items[i];
    console.log(item);
    item.onclick = function () {
        window.location = item.href;
    }
}

function remove_notification(button) {
    let notification_id = button.dataset.notificationId;
    $.ajax({
        url: '/ajax/remove_notification/' + notification_id,
        dataType: 'json',
        success: function (data) {
            if(data['status'] === 'OK') {
                let notification = document.getElementById(notification_id);
                notification.remove();
            }
        }
    });
}
