from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

import json
import time as timestamp
from multiprocessing import Process
from channels.layers import get_channel_layer
from requests import request

from Registration.models import LogBet
from .views import add_duel_room, get_duel_rooms, update_biggest_win
from .models import Bet, Player, BiggestWin
from ClashBet import settings


class RoyaleAPI:
    def __init__(self):
        self.rate_limit_exceeded = False
        self.token = settings.ROYALE_API_TOKEN

    def royaleapi_request(self, url):
        headers = {
            'auth': self.token
        }
        if self.rate_limit_exceeded:
            timestamp.sleep(1)
        response = request("GET", url, headers=headers)
        print(response.text)
        # while response is None:
        #     try:
        #         response_raw = request("GET", url, headers=headers)
        #         if response_raw.headers['x-ratelimit-remaining'] == '0':
        #             self.rate_limit_exceeded = True
        #         else:
        #             self.rate_limit_exceeded = False
        #         print(response_raw)
        #         response = response_raw.json()
        #     except ConnectionError:
        #         pass
        return json.loads(response.text)


class DuelRoom:
    class DuelPlayer:
        def __init__(self, player):
            self.player = player
            self.in_clan = False
            self.ready = False

    def __init__(self, bet_id):
        self.bet_id = bet_id
        self.duel_player = None
        self.duel_opponent = None
        self.check_ongoing = False
        self.player_results = None
        self.opponent_results = None
        self.auto_check_process = None

    def results_correct(self):
        if self.player_results == 'win' and self.opponent_results == 'lose' or \
                self.player_results == 'lose' and self.opponent_results == 'win' or \
                self.player_results == 'draw' and self.opponent_results == 'draw':
            response = True
        else:
            response = False
        if self.player_results is None or self.opponent_results is None:
            response = None
        return response

    def get_results(self):
        response = None
        winner = None
        loser = None
        if self.player_results == 'win':
            response = {'battle_result': 'complete', 'winner': self.duel_player.player.CRnickname}
            winner = self.duel_player.player
            loser = self.duel_opponent.player
        elif self.opponent_results == 'win':
            response = {'battle_result': 'complete', 'winner': self.duel_opponent.player.CRnickname}
            winner = self.duel_opponent.player
            loser = self.duel_player.player
        elif self.player_results == 'draw':
            response = {'battle_result': 'draw'}
        return response, winner, loser

    def save(self):
        add_duel_room(self.bet_id, self)
        return True

    def is_set(self):
        return True if self.duel_player is not None and self.duel_opponent is not None else False

    def ready2go(self):
        return True if self.duel_player.ready and self.duel_opponent.ready else False


royale_api = RoyaleAPI()


def draw(duel_room, bet=None):
    bet = Bet.objects.get(id=duel_room.bet_id) if duel_room is not None else bet
    bet.finished = True
    bet.ongoing = False
    bet.player.balance += bet.cost
    bet.opponent.balance += bet.cost
    bet.opponent.last_bets.add(LogBet.objects.create(profit=bet.cost, opponent=bet.player.user.username))
    bet.player.last_bets.add(LogBet.objects.create(profit=-bet.cost, opponent=bet.opponent.user.username))
    bet.player.last_win = False
    bet.opponent.last_win = False
    bet.player.win_streak = 0
    bet.opponent.win_streak = 0
    process = duel_room.auto_check_process
    bet.save()
    bet.opponent.save()
    bet.player.save()
    if process is not None:
        process.terminate()
        print('auto check process terminated')
    data = {
        'bet_id': str(bet.id),
        'message': 'DUEL_ENDED'
    }
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'bets_update',
        {
            'type': 'send_message',
            'data': data
        }
    )


def end_duel(duel_room, winner, loser, bet=None):
    bet = Bet.objects.get(id=duel_room.bet_id) if duel_room is not None else bet
    update_biggest_win()
    biggest_win = BiggestWin.objects.get(id=1)
    biggest_win.is_none = False
    comission = 1 if bet.acception_timestamp < winner.no_comission_bonus_expiration_timestamp \
        or winner.daily_offer_activated else 0.9
    profit = bet.cost * 2 * comission
    biggest_win.win_amount = profit
    biggest_win.player = winner
    biggest_win.save()
    winner.balance += profit
    bet.finished = True
    bet.ongoing = False
    bet.winner = winner
    winner.last_bets.add(LogBet.objects.create(profit=bet.cost, opponent=loser.user.username))
    loser.last_bets.add(LogBet.objects.create(profit=-bet.cost, opponent=winner.user.username))
    winner.last_win = True
    if winner.last_win:
        winner.win_streak += 1
    if winner.max_win < profit:
        winner.max_win = profit
    winner.wins += 1
    winner.profit += profit
    loser.last_win = False
    loser.win_streak = 0
    winner.save()
    loser.save()
    bet.save()
    process = duel_room.auto_check_process
    if process is not None:
        process.terminate()
        print('auto check process terminated')
    channel_layer = get_channel_layer()
    data = {
        'bet_id': str(bet.id),
        'message': 'DUEL_ENDED'
    }
    async_to_sync(channel_layer.group_send)(
        'bets_update',
        {
            'type': 'send_message',
            'data': data
        }
    )


class BetsConsumer(WebsocketConsumer):

    def connect(self):
        print('bets websocket:', self.scope['user'].username, 'connected')
        async_to_sync(self.channel_layer.group_add)(
            'bets_update',
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        print('bets websocket:', self.scope['user'].username, 'disconnected')
        async_to_sync(self.channel_layer.group_discard)(
            'bets_update',
            self.channel_name
        )

    def receive(self, text_data):
        data = json.loads(text_data)
        print('new message:', data)
        bet_id = data['bet_id']
        if data['message'] == 'BET_CREATED' or data['message'] == 'DUEL_STARTED' or data['message'] == 'DUEL_ENDED':
            async_to_sync(self.channel_layer.group_send)(
                'bets_update',
                {
                    'type': 'send_message',
                    'data': data
                }
            )
        elif data['message'] == 'DUEL_CREATED' or data['message'] == 'DUEL_ACCEPTION_REQUEST':
            bet = Bet.objects.get(id=bet_id)
            player = Player.objects.get(user__username=self.scope['user'].username)
            duel_rooms = get_duel_rooms()
            duel_room = DuelRoom(bet_id)

            if bet_id not in duel_rooms.keys():
                duel_rooms[bet_id] = None
            if duel_rooms[bet_id] is None:
                bet.player = player
                bet.save()
                duel_rooms[bet_id] = player
                async_to_sync(self.channel_layer.group_add)(
                    bet_id,
                    self.channel_name
                )
            elif duel_rooms[bet_id] is not None and player != duel_rooms[bet_id]:
                opponent = player
                if opponent.balance >= bet.cost:
                    bet.opponent = opponent
                    bet.save()
                    player = duel_rooms[bet_id]
                    duel_room.duel_player = duel_room.DuelPlayer(player)
                    duel_room.duel_opponent = duel_room.DuelPlayer(opponent)
                    add_duel_room(bet_id, duel_room)
                    async_to_sync(self.channel_layer.group_add)(
                        bet_id,
                        self.channel_name
                    )
            if duel_room.is_set():
                async_to_sync(self.channel_layer.group_send)(
                    bet_id,
                    {
                        'type': 'send_message',
                        'data': {
                            'bet_id': bet_id,
                            'message': 'DUEL_READY'
                        }
                    }
                )
                bet.ongoing = True
                bet.expiration_ongoing_timestamp = round(timestamp.time()) + 900
                bet.opponent.balance -= bet.cost
                bet.opponent.save()
                bet.save()
                add_duel_room(bet_id, duel_room)
        elif data['message'] == 'DUEL_CONNECT':

            duel_rooms = get_duel_rooms()
            duel_room = duel_rooms[bet_id]

            duel_room.duel_player.ready = True if \
                self.scope['user'].username == duel_room.duel_player.player.user.username \
                or duel_room.duel_player.ready else False
            duel_room.duel_opponent.ready = True if \
                self.scope['user'].username == duel_room.duel_opponent.player.user.username \
                or duel_room.duel_opponent.ready else False

            async_to_sync(self.channel_layer.group_add)(
                bet_id,
                self.channel_name
            )

            try:
                Bet.objects.get(id=int(bet_id))
            except Bet.DoesNotExist:
                response = {'battle_result': 'expired'}
                async_to_sync(self.channel_layer.group_send)(
                    bet_id,
                    {
                        'type': 'duel_message',
                        'data': response,
                    }
                )

            if duel_room.ready2go() and not duel_room.check_ongoing:
                duel_room.check_ongoing = True
                duel_room.auto_check_process = Process(target=self.check_timeout, args=[360, duel_room])
                duel_room.auto_check_process.start()
                print('done !')

        elif data['message'] == 'SELF_CHECK':
            duel_rooms = get_duel_rooms()
            duel_room = duel_rooms[bet_id]
            bet = Bet.objects.get(id=duel_room.bet_id)
            response = self.self_check(bet)

            print('response:', response)
            async_to_sync(self.channel_layer.group_send)(
                bet_id,
                {
                    'type': 'duel_message',
                    'data': response,
                }
            )

            print('done !')
        elif data['message'] == 'DUEL_RESULTS':
            duel_rooms = get_duel_rooms()
            duel_room = duel_rooms[bet_id]
            results = {
                0: 'win',
                1: 'lose',
                2: 'draw'
            }

            if self.scope['user'].username == duel_room.duel_player.player.user.username:
                duel_room.player_results = results[data['results'].index(True)]
            if self.scope['user'].username == duel_room.duel_opponent.player.user.username:
                duel_room.opponent_results = results[data['results'].index(True)]
            duel_room.save()

            results_correct = duel_room.results_correct()

            if results_correct:
                response, winner, loser = duel_room.get_results()
                if response != {'battle_result': 'draw'}:
                    end_duel(duel_room, winner, loser)
                elif response == {'battle_result': 'draw'}:
                    draw(duel_room)
                async_to_sync(self.channel_layer.group_send)(
                    bet_id,
                    {
                        'type': 'duel_message',
                        'data': response,
                    }
                )
            elif results_correct is False:
                data = {
                    'message': 'corrupted_data'
                }
                print('CORRUPTED FUCKING DATA BITCH')
                async_to_sync(self.channel_layer.group_send)(
                    bet_id,
                    {
                        'type': 'duel_message',
                        'data': data,
                    }
                )
                duel_room.auto_check_process.terminate()
                print('executing automatic check!')
                self.automatic_check(duel_room)

    def send_message(self, event):
        message = event['data']
        self.send(text_data=json.dumps({
            'data': message
        }))

    def check_timeout(self, timeout, duel_room):
        print('sleeping ' + str(timeout) + ' seconds before auto check...')
        timestamp.sleep(timeout)
        print('executing automatic check!')
        self.automatic_check(duel_room)
        return True

    @staticmethod
    def automatic_check(duel_room):
        bet = Bet.objects.get(id=duel_room.bet_id)
        response = {'battle_result': 'ongoing'}
        channel_layer = get_channel_layer()
        while response == {'battle_result': 'ongoing'}:

            player_url = "https://api.royaleapi.com/player/" + bet.player.hashtag + '/battles'
            opponent_url = "https://api.royaleapi.com/player/" + bet.opponent.hashtag + '/battles'

            player_data = royale_api.royaleapi_request(player_url)
            opponent_data = royale_api.royaleapi_request(opponent_url)

            if not duel_room.duel_player.in_clan:
                player_url_clan = "https://api.royaleapi.com/player/" + bet.player.hashtag
                player_clan_data = royale_api.royaleapi_request(player_url_clan)
                print('pcd:', player_clan_data)
                if player_clan_data['clan']['tag'] == bet.clan.hashtag:
                    duel_room.duel_player.in_clan = True

            if not duel_room.duel_opponent.in_clan:
                opponent_url_clan = "https://api.royaleapi.com/player/" + bet.opponent.hashtag
                opponent_clan_data = royale_api.royaleapi_request(opponent_url_clan)
                print('ocd:', opponent_clan_data)
                if opponent_clan_data['clan']['tag'] == bet.clan.hashtag:
                    duel_room.duel_opponent.in_clan = True

            if type(player_data) is list:
                player_last_battle = {
                    'winner': player_data[0]['winner'],
                    'opponent_tag': player_data[0]['opponent'][0]['tag'],
                    'utc_timestamp': player_data[0]['utcTime'],
                }
            else:
                player_last_battle = {
                    'winner': player_data['winner'],
                    'opponent_tag': player_data['opponent'][0]['tag'],
                    'utc_timestamp': player_data['utcTime'],
                }
            if type(opponent_data) is list:
                opponent_last_battle = {
                    'winner': opponent_data[0]['winner'],
                    'opponent_tag': opponent_data[0]['opponent'][0]['tag'],
                    'utc_timestamp': opponent_data[0]['utcTime'],
                }
            else:
                opponent_last_battle = {
                    'winner': opponent_data['winner'],
                    'opponent_tag': opponent_data['opponent'][0]['tag'],
                    'utc_timestamp': opponent_data['utcTime'],
                }
            print('plb:', player_last_battle)
            print('olb:', opponent_last_battle)

            if player_last_battle['opponent_tag'].lower() == bet.opponent.hashtag.lower() \
                    and opponent_last_battle['opponent_tag'].lower() == bet.player.hashtag.lower() \
                    and player_last_battle['utc_timestamp'] > bet.acception_timestamp \
                    and opponent_last_battle['utc_timestamp'] > bet.acception_timestamp:
                if player_last_battle['winner'] > opponent_last_battle['winner']:
                    response = {'battle_result': 'complete', 'winner': bet.player.CRnickname}
                    end_duel(duel_room, bet.player, bet.opponent)
                elif player_last_battle['winner'] < opponent_last_battle['winner']:
                    response = {'battle_result': 'complete', 'winner': bet.opponent.CRnickname}
                    end_duel(duel_room, bet.opponent, bet.player)
                else:
                    draw(duel_room)
                    response = {
                        'battle_result': 'draw'
                    }

            elif bet.expiration_ongoing_timestamp - timestamp.time() <= 0:
                if duel_room.duel_player.in_clan and not duel_room.duel_opponent.in_clan:
                    response = {'battle_result': 'not_played',
                                'not_joined_clan': bet.opponent.CRnickname,
                                'winner': bet.player.CRnickname}
                    end_duel(duel_room, bet.player, bet.opponent)
                elif duel_room.duel_opponent.in_clan and not duel_room.duel_player.in_clan:
                    response = {'battle_result': 'not_played',
                                'not_joined_clan': bet.player.CRnickname,
                                'winner': bet.opponent.CRnickname}
                    end_duel(duel_room, bet.opponent, bet.player)
                else:
                    bet.finished = True
                    bet.unfinished = True
                    bet.save()
                    response = {'battle_result': 'time_over'}
            else:
                response = {'battle_result': 'ongoing'}
            print('response:', response)
            async_to_sync(channel_layer.group_send)(
                str(bet.id),
                {
                    'type': 'duel_message',
                    'data': response,
                }
            )
            print('waiting...')
            timestamp.sleep(10)

    @staticmethod
    def self_check(bet):
        response = None

        player_url = "https://api.royaleapi.com/player/" + bet.player.hashtag + '/battles'
        opponent_url = "https://api.royaleapi.com/player/" + bet.opponent.hashtag + '/battles'

        player_data = royale_api.royaleapi_request(player_url)
        opponent_data = royale_api.royaleapi_request(opponent_url)

        if type(player_data) is list:
            player_last_battle = {
                'winner': player_data[0]['winner'],
                'opponent_tag': player_data[0]['opponent'][0]['tag'],
                'utc_timestamp': player_data[0]['utcTime'],
            }
        else:
            player_last_battle = {
                'winner': player_data['winner'],
                'opponent_tag': player_data['opponent'][0]['tag'],
                'utc_timestamp': player_data['utcTime'],
            }
        if type(opponent_data) is list:
            opponent_last_battle = {
                'winner': opponent_data[0]['winner'],
                'opponent_tag': opponent_data[0]['opponent'][0]['tag'],
                'utc_timestamp': opponent_data[0]['utcTime'],
            }
        else:
            opponent_last_battle = {
                'winner': opponent_data['winner'],
                'opponent_tag': opponent_data['opponent'][0]['tag'],
                'utc_timestamp': opponent_data['utcTime'],
            }

        print('plb:', player_last_battle)
        print('olb:', opponent_last_battle)

        # print('bet_info:', player_last_battle['opponent_tag'].lower() == bet.opponent.hashtag.lower(),
        #       opponent_last_battle['opponent_tag'].lower() == bet.player.hashtag.lower(),
        #       player_last_battle['utc_timestamp'] > bet.acception_timestamp,
        #       opponent_last_battle['utc_timestamp'] > bet.acception_timestamp)
        # print('bet_acception_timestamp:', bet.acception_timestamp)

        if player_last_battle['opponent_tag'].lower() == bet.opponent.hashtag.lower() \
                and opponent_last_battle['opponent_tag'].lower() == bet.player.hashtag.lower() \
                and player_last_battle['utc_timestamp'] > bet.acception_timestamp \
                and opponent_last_battle['utc_timestamp'] > bet.acception_timestamp:
            if player_last_battle['winner'] > opponent_last_battle['winner']:
                response = {'battle_result': 'complete', 'winner': bet.player.CRnickname}
                end_duel(None, bet.player, bet.opponent, bet)
            elif player_last_battle['winner'] < opponent_last_battle['winner']:
                response = {'battle_result': 'complete', 'winner': bet.opponent.CRnickname}
                end_duel(None, bet.opponent, bet.player, bet)
            else:
                response = {'battle_result': 'draw'}
                draw(bet)
        return response

    def duel_message(self, event):
        new_duel = event['data']
        self.send(text_data=json.dumps({
            'data': new_duel,
        }))
