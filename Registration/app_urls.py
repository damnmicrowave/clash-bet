from django.conf.urls import url
from django.contrib.auth import views as auth

from .views import logout, register, change_email, change_password

app_name = "Registration"

urlpatterns = [
    url(r'^login/$', auth.LoginView.as_view(
        template_name='registration/login.html'
    ), name='login'),
    url(r'^register/$', register, name='register'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^change_email/$', change_email, name='change_email'),
    url(r'^change_password/$', change_password, name='change_password'),
]
