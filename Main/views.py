from django.shortcuts import render

from Main.models import Feature
from Registration.models import Player


def startpage(request):
    features = Feature.objects.all()
    return render(request, 'main/startpage.html', {'features': features})


def faq(request):
    player = None
    try:
        player = Player.objects.get(user__username=request.user.username)
    except Player.DoesNotExist:
        pass
    return render(request, 'main/faq.html', {'player': player})


def tos(request):
    player = None
    try:
        player = Player.objects.get(user__username=request.user.username)
    except Player.DoesNotExist:
        pass
    return render(request, 'main/tos.html', {'player': player})
