function setup_animation () {
    let canvas = document.getElementById('background');
    let W = canvas.width = window.innerWidth;
    let H = canvas.height = window.innerHeight;
    let ctx = canvas.getContext('2d');

    function Square(x, y, w, a, dx, pos, c) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.a = a;
        this.pos = pos;
        this.dx = dx;
        this.color = c;

        this.draw = function () {
            ctx.beginPath();
            if (this.color === 'blue') {
                ctx.fillStyle = '#00b2ff';
            }
            else {
                ctx.fillStyle = '#ff4d00';
            }
            ctx.globalAlpha = this.a;
            ctx.rect(this.x, this.y, this.w, this.w);
            ctx.fill();
        };

        this.update = function () {
            if (this.color === 'blue'){
                this.x += this.dx;
            }
            else {
                this.x -= this.dx;
            }
            if (this.a >= 0.005){
                this.a -= 0.005;
            }
            else {
                if (this.color === 'blue'){
                    delete blue_squares[this.pos];
                    create_square(this.pos, this.color)
                }
                else {
                    delete red_squares[this.pos];
                    create_square(this.pos, this.color)
                }

            }
            this.draw();
        }
    }

    let blue_squares = {};
    let red_squares = {};

    function create_square(i, color){
        let x = null;
        let w = Math.random() * (W/10 - W/100) + W/100;
        let dx = Math.random() * (W/500 - W/1000) + W/1000;
        let y = Math.random() * H;
        let a = Math.random() * (0.6 - 0.1) + 0.05;
        if (color === 'blue'){
            x = -w;
        }
        else {
            x = W;
        }
        if (color === 'blue') {
            blue_squares[i] = new Square(x, y, w, a, dx, i, color);
        }
        else {
            red_squares[i] = new Square(x, y, w, a, dx, i, color);
        }
    }

    for (let i = 0; i < 20; i++){
        create_square(i, 'blue');
        create_square(i, 'red');
    }

    function animate() {
        ctx.clearRect(0, 0, W, H);
        ctx.save();
        for (let i = 0; i < Object.keys(blue_squares).length; i++){
            blue_squares[i].update();
            red_squares[i].update();
        }
        ctx.restore();
        requestAnimationFrame(animate);
    }

    requestAnimationFrame(animate);
}

window.onload = function () {
    setup_animation();
};

window.addEventListener('resize', setup_animation);