from django.db import models

from Registration.models import Player


class OfferParticipant(models.Model):
    player = models.ForeignKey(Player, default=1, on_delete=models.CASCADE)
    number = models.IntegerField()

    def __str__(self):
        return str(self.id) + ' | ' + self.player.user.username + ' | ' + str(self.number)


class DailyOffer(models.Model):
    participants = models.ManyToManyField(OfferParticipant, default=1)
    time_left = models.IntegerField(default=86400)

    def __str__(self):
        return 'Daily Offer'

    class Meta:
        verbose_name_plural = 'Daily Offer'
