from django.conf.urls import url

from .views import main, make_a_bet, waiting, duel, get_bet_info, appeal, get_battle_status, remove_notification

app_name = "Bets"

urlpatterns = [
    url(r'^$', main, name='main'),
    url(r'^make-a-bet/$', make_a_bet, name='make-a-bet'),
    url(r'^waiting/(?P<bet_id>\d+)/$', waiting, name='waiting'),
    url(r'^duel/(?P<bet_id>\d+)/$', duel, name='duel'),
    url(r'^appeal/(?P<bet_id>\d+)/$', appeal, name='appeal'),
    url(r'ajax/get_bet_info/(?P<bet_id>\d+)/$', get_bet_info),
    url(r'ajax/get_battle_status/$', get_battle_status),
    url(r'ajax/remove_notification/(?P<notification_id>\d+)$', remove_notification),
]
