from django.contrib import admin

from .models import Feature, Notification

admin.site.register(Feature)
admin.site.register(Notification)
