from django.contrib import admin

from Offers.models import DailyOffer, OfferParticipant

admin.site.register(DailyOffer)
admin.site.register(OfferParticipant)
