from django.shortcuts import render, redirect

from Registration.models import Player


def profile(request, username):
    player = Player.objects.get(user=request.user)
    return render(request, 'lk/lk.html', {'player': player})


def bets(request, username):
    player = Player.objects.get(user=request.user)
    return render(request, 'lk/bets.html', {'player': player})


def toggle_background(request):
    player = Player.objects.get(user=request.user)
    player.background = False if player.background else True
    player.save()
    return redirect(request.user.username + '/profile')
