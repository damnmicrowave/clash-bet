from django.contrib.auth.models import User
from django.db import models


class Clan(models.Model):
    hashtag = models.CharField(max_length=32, default='')
    name = models.CharField(max_length=32, default='')

    def __str__(self):
        return str(self.id) + ' ' + self.name + ' #' + self.hashtag


class LogBet(models.Model):
    profit = models.FloatField(default=0)
    opponent = models.CharField(max_length=32, default='')

    def __str__(self):
        return str(self.id) + ' | ' + self.opponent + ' | ' + str(self.profit)


class Player(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    hashtag = models.CharField(max_length=20)
    balance = models.FloatField(default=0.0)
    CRnickname = models.CharField(max_length=45, default='oops')
    last_bets = models.ManyToManyField(LogBet, default=None)
    profit = models.FloatField(default=0)
    max_win = models.FloatField(default=0)
    wins = models.IntegerField(default=0)
    win_streak = models.IntegerField(default=0)
    last_win = models.BooleanField(default=False)
    daily_offer_activated = models.BooleanField(default=False)
    no_comission_bonus_expiration_timestamp = models.BigIntegerField(default=0)
    background = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id) + ' | ' + self.user.username + ' | ' + str(self.balance)
