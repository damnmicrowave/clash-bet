from django.conf.urls import url

from .views import profile, bets, toggle_background

app_name = "LK"

urlpatterns = [
    url(r'^(?P<username>\S+)/profile$', profile, name='profile'),
    url(r'^(?P<username>\S+)/bets$', bets, name='bets'),
    url(r'^toggle_background$', toggle_background, name='toggle_background'),
]
