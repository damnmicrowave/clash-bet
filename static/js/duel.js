let timer = document.getElementById('timer');
let bet_id = timer.dataset.betId;
let info = document.getElementById('info');
let result = document.getElementById('result');
let result_btn = document.getElementById('send-result');
let timer_interval = null;
let win = document.getElementById('id_win');
let lose = document.getElementById('id_lose');
let draw = document.getElementById('id_draw');
let results_not_sent = true;
let results_checked = info.dataset.checked;

function calc_timer() {
    let curr_timestamp = Math.floor((new Date()).getTime() / 1000);
    let time_left = parseInt(timer.dataset.expirationTimestamp) - curr_timestamp;
    let minutes_left = Math.floor(time_left / 60).toString();
    let seconds_left = (time_left % 60).toString();
    if (seconds_left.length === 1) {
        seconds_left = '0' + seconds_left;
    }
    if (minutes_left.length === 1) {
        minutes_left = '0' + minutes_left;
    }
    if (time_left < 900 && time_left >= 540 && results_not_sent && results_checked === 'None') {
        info.innerHTML = 'Если Вы завершили дуэль, пожалуйста, отправьте её результат ниже.<br><br>' +
            'При несоответствии результатов, присланных Вами, с результатами противника, наша система проверит итоги ' +
            'боя автоматически.';
        result.classList.remove('d-none');
    }
    if (time_left < 540) {
        info.innerHTML = 'Один из игроков не подтвердил результаты боя. Наша система определит их автоматически.'
    }
    if (time_left > 0) {
        timer.innerHTML = minutes_left + ':' + seconds_left;
    } else {
        clearInterval(timer_interval);
    }
}

function remove_bet() {
    betsSocket.send(
        JSON.stringify({
            'bet_id': bet_id,
            'message': 'DUEL_ENDED'
        })
    );
}

function get_battle_status () {
    let bet_id = timer.dataset.betId;
    $.ajax({
        url: '/ajax/get_battle_status/',
        data: {
            'bet_id': bet_id
        },
        dataType: 'json',
        success: function (data) {
            if (data['battle_status'] === 'ongoing') {
                timer_interval = setInterval(calc_timer, 1000);
            }
            else if (data['battle_status'] === 'not_started') {
                info.innerHTML = 'При создании дуэли возникла ошибка<br>' +
                'Одна из причин: у противника недостаточно средств для принятия ставки<br>' +
                'Ваши траты были возмещены, вы можете <a href="/">вернуться на главную</a>';
                setTimeout(get_battle_status, 1000)
            }
            else if (data['battle_status'] === 'complete') {
                remove_bet();
                info.innerHTML = data['winner'] + ', поздравляем с победой!<br>' +
                    'Дуэль окончена, вы можете <a href="/">вернуться на главную</a>';
            }
            else if (data['battle_status'] === 'time_over') {
                remove_bet();
                info.innerHTML =
                    'Время на дуэль истекло.<br>' +
                    'Автоматическая проверка результатов остановлена.<br>' +
                    'Если Вы завершили бой после истечении времени на дуэль, используйте ' +
                    '<a onclick="get_battle_status()">проверку результатов вручную.</a><br><br>' +
                    'Если Вы не смогли сыграть бой по причине неявки оппонента Вы можете ' +
                    '<a href="http://' + window.location.host + '/appeal/' + bet_id + '">подать на апелляцию</a>';
            }
        },
        fail: function () {
            info.innerHTML = 'Мы не можем получить ответ от сервера<br>' +
                'Проверьте ваше подключение к Интернету и обновите страицу';
            setTimeout(get_battle_status, 1000)
        }
    });
}

function get_battle_result () {
    betsSocket.send(
        JSON.stringify({
            'bet_id': bet_id,
            'message': 'SELF_CHECK'
        })
    );
}

result_btn.onclick = function () {
    betsSocket.send(
        JSON.stringify({
            'bet_id': bet_id,
            'message': 'DUEL_RESULTS',
            'results': [win.checked, lose.checked, draw.checked]
        })
    );
    results_not_sent = false;
    info.innerHTML =
        'Результаты отправлены!<br>' +
        'Ждём результаты противника...';
    result.remove();
};

get_battle_status();

let betsSocket = new WebSocket('ws://' + window.location.host + '/ws/bets/');

betsSocket.onopen = function () {
    betsSocket.send(
        JSON.stringify({
            'bet_id': bet_id,
            'message': 'DUEL_STARTED'
        })
    );
    betsSocket.send(
        JSON.stringify({
            'bet_id': bet_id,
            'message': 'DUEL_CONNECT'
        })
    );
};

betsSocket.onclose = function () {
    console.error('ws connection closed !');
};

betsSocket.onmessage = function (e) {
    let data = JSON.parse(e.data)['data'];
    console.log(data);
    if (data['battle_result'] === 'draw') {
        info.innerHTML = 'Ничья :/<br>' +
            'Мы вернули вам все потраченные на ставку средства.<br><br>' +
            'Дуэль окончена, вы можете <a href="/">вернуться на главную</a>';
        clearInterval(timer_interval);
    }
    else if (data['battle_result'] === 'not_played') {
        remove_bet();
        info.innerHTML = data['not_joined_clan'] + ' не вошёл в клан.<br>' +
            data['winner'] + ', вам присуждается автоматическая победа.<br>' +
            'Дуэль окончена, вы можете <a href="/">вернуться на главную</a>.';
        clearInterval(timer_interval);
    }
    else if (data['battle_result'] === 'complete') {
        remove_bet();
        info.innerHTML = data['winner'] + ', поздравляем с победой!<br>' +
            'Дуэль окончена, вы можете <a href="/">вернуться на главную</a>';
        clearInterval(timer_interval);
    }
    else if (data['battle_result'] === 'time_over') {
        remove_bet();
        info.innerHTML =
            'Время на дуэль истекло.<br>' +
            'Автоматическая проверка результатов остановлена.<br>' +
            'Если Вы завершили бой после истечении времени на дуэль, используйте ' +
            '<a onclick="get_battle_result()">проверку результатов вручную</a>.<br><br>' +
            'Если Вы не смогли сыграть бой по причине неявки оппонента Вы можете ' +
            '<a href="http://' + window.location.host + '/appeal/' + bet_id + '">подать на апелляцию</a>.';
        clearInterval(timer_interval);
    }
    else if (data['message'] === 'corrupted_data') {
        clearInterval(timer_interval);
        info.innerHTML =
            'Опаньки!<br>' +
            'Один из игроков ввёл некорректные данные.<br>' +
            'Запущена автоматическая проверка результатов...';
        result.classList.add('d-none');
    }
};
