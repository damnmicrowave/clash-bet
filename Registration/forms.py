from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, label='Почтовый ящик', required=True, widget=forms.EmailInput())
    hashtag = forms.CharField(max_length=32, label='CR Hashtag', required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'hashtag', 'password1', 'password2')
