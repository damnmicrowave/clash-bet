import datetime
import math
import time
import threading
import random

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from Main.models import Notification
from Offers.models import DailyOffer, OfferParticipant
from Registration.models import Player


def seconds_until_end_of_today():
    time_delta = datetime.datetime.combine(
        datetime.datetime.now().date() + datetime.timedelta(days=1), datetime.datetime.strptime("0000", "%H%M").time()
    ) - datetime.datetime.now()
    return time_delta.seconds - 10800


def complete_daily_offer():
    seconds_to_midnight = seconds_until_end_of_today()
    time.sleep(seconds_to_midnight)
    participants_count = OfferParticipant.objects.count()
    winners_count = math.floor(participants_count * 0.5)
    winners_numbers = random.sample(range(1, participants_count + 1), winners_count)
    players = Player.objects.filter(daily_offer_activated=True)
    for player in players:
        player.daily_offer_activated = False
        player.save()
    for num in winners_numbers:
        winner = OfferParticipant.objects.get(number=num)
        winner.player.daily_offer_activated = True
        winner.player.save()
        Notification.objects.create(player=winner.player,
                                    text='Поздравляем! Вы стали победителем в ежедневной акции',
                                    type='success')
    OfferParticipant.objects.all().delete()
    complete_daily_offer()


t = threading.Thread(target=complete_daily_offer, args=(), kwargs={})
t.setDaemon(True)
t.start()


@login_required(login_url='/startpage')
def daily_offer(request):
    offer = DailyOffer.objects.get(id=1)
    player = Player.objects.get(user__username=request.user.username)
    notifications = Notification.objects.filter(player=player)
    try:
        offer_participant = OfferParticipant.objects.get(player=player)
    except OfferParticipant.DoesNotExist:
        offer_participant = None
    offer.time_left = math.floor(seconds_until_end_of_today())
    offer.save()
    return render(request, 'offers/daily_offer.html', {
        'player': player,
        'offer': offer,
        'offer_participant': offer_participant,
        'notifications': notifications
    })


@login_required(login_url='/startpage')
def daily_offer_take_part(request):
    player = Player.objects.get(user__username=request.user.username)
    offer = DailyOffer.objects.get(id=1)
    try:
        offer_participant = OfferParticipant.objects.get(player=player)
    except OfferParticipant.DoesNotExist:
        offer_participant = None
    if player.balance >= 25 and offer_participant is None:
        player.balance -= 25
        player.save()
        try:
            number = offer.participants.last().number + 1
        except AttributeError:
            number = 1
        participant = OfferParticipant.objects.update_or_create(player=player, number=number)
        offer.participants.add(participant[0])
        offer.save()
        Notification.objects.create(player=player, text='Вы участвуете', type='success')
    else:
        Notification.objects.create(player=player,
                                    text='Недостаточно средств для участия в ежедневной акции', type='error')
    return redirect('/daily_offer')


@login_required(login_url='/startpage')
def coupons(request):
    player = Player.objects.get(user__username=request.user.username)
    notifications = Notification.objects.filter(player=player)
    now = round(time.time())
    time_left = player.no_comission_bonus_expiration_timestamp - now
    if time_left < 0:
        time_left = 0
    return render(request, 'offers/coupons.html', {
        'player': player,
        'time_left': time_left,
        'notifications': notifications
    })


@login_required(login_url='/startpage')
def buy_coupon(request, days):
    days = int(days)
    coupons_cost = {
        3: 1490,
        7: 2790,
        30: 9900
    }
    coupons_endings = {
        3: ' дня',
        7: ' дней',
        30: ' дней'
    }
    if days not in coupons_cost.keys():
        redirect('/coupons')
    player = Player.objects.get(user__username=request.user.username)
    if not player.balance >= coupons_cost[days]:
        Notification.objects.create(player=player,
                                    text='Ошибка - недостаточно средств',
                                    type='error')
        redirect('/coupons')
    else:
        if player.no_comission_bonus_expiration_timestamp < time.time():
            player.no_comission_bonus_expiration_timestamp = round(time.time())
        player.balance -= coupons_cost[days]
        player.no_comission_bonus_expiration_timestamp += days * 86400
        player.save()
        Notification.objects.create(
            player=player,
            text='Бонус "без комиссии" на ' + str(days) + coupons_endings[days] + ' активирован!',
            type='success'
        )
    return redirect('/coupons')
