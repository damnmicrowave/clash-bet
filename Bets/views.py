import datetime
import time as timestamp

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect

from Bets.models import Bet, ExpiredBet, Appeal, UploadedFile, BiggestWin
from Main.models import Notification
from Registration.models import Player, Clan

duel_rooms = dict()


def add_duel_room(bet_id, duel_room):
    duel_rooms[bet_id] = duel_room


def get_duel_rooms():
    return duel_rooms


def delete_bets():
    expired_bets = Bet.objects.filter(expiration__lt=datetime.datetime.now(), ongoing=False)
    for bet in expired_bets:
        ExpiredBet.objects.create(
            cost=bet.cost,
            player=bet.player,
            clan=bet.clan,
            expiration_timestamp=bet.expiration_timestamp)
        bet.player.balance += bet.cost
        bet.player.save()
        bet.delete()


def update_biggest_win():
    b_w = BiggestWin.objects.get(id=1)
    curr_date = datetime.datetime.now().date()
    if b_w.date != curr_date:
        b_w.date = curr_date
        b_w.is_none = True
        b_w.save()


@login_required(login_url='/startpage/')
def main(request):
    delete_bets()
    update_biggest_win()
    player = Player.objects.get(user=request.user)
    biggest_win = BiggestWin.objects.get(id=1)
    bets = Bet.objects.filter(finished=False).order_by('cost')
    notifications = Notification.objects.filter(player=player)
    for bet in bets:
        bet.time_left = bet.expiration_timestamp - round(timestamp.time())
        bet.save()
    try:
        bet = Bet.objects.get(player=player, finished=False)
        if bet.ongoing:
            return redirect('/duel/' + str(bet.id))
        else:
            return redirect('/waiting/' + str(bet.id))
    except Bet.DoesNotExist:
        pass
        return render(request, 'main/main.html',
                      {
                          'server_time': timestamp.time(),
                          'player': player,
                          'bets': bets,
                          'biggest_win': biggest_win,
                          'notifications': notifications
                      })


@login_required(login_url='/startpage/')
def make_a_bet(request):
    player = Player.objects.get(user=request.user)
    if request.method == 'POST':

        cost, time = None, None
        try:
            cost = float(request.POST.get('sum', None))
            time = int(request.POST.get('time', None)) * 60
            if cost > player.balance:
                Notification.objects.create(player=player, type='error', text='Ошибка - недостаточно средств')
            if not 300 <= time <= 1800:
                Notification.objects.create(player=player, type='error', text='Ошибка - неверно указано время')
            if cost < 10:
                Notification.objects.create(player=player, type='error',
                                            text='Ошибка - неверно указана стоимость')
            if len(list(Bet.objects.filter(player=player, finished=False))) > 0:
                Notification.objects.create(player=player, type='error',
                                            text='Ошибка - у вас уже есть незавершённые ставки')
        except ValueError:
            Notification.objects.create(player=player, type='error',
                                        text='Ошибка - неверно указаны время или стоимость')

        if len(Notification.objects.filter(player=player, type='error')) == 0:
            expiration_time = timestamp.time() + time
            clan = Clan.objects.get(id=1)
            bet = Bet.objects.create(
                cost=cost,
                time=time,
                expiration=datetime.datetime.utcfromtimestamp(expiration_time),
                expiration_timestamp=expiration_time,
                opponent=player,
                player=player,
                winner=player,
                clan=clan
            )
            player.balance -= cost
            player.save()

            return redirect('/waiting/' + str(bet.id))

    return redirect('/')


@login_required(login_url='/startpage/')
def waiting(request, bet_id):
    player = Player.objects.get(user__username=request.user.username)
    try:
        bet = Bet.objects.get(id=bet_id)
        bet.time_left = bet.expiration_timestamp - round(timestamp.time())
        bet.save()
        if request.user == bet.player.user:
            return render(request, 'bets/waiting.html', {'bet': bet})
        else:
            return redirect('/')
    except Bet.DoesNotExist:
        Notification.objects.create(player=player,
                                    text='Cтавки, которую Вы попытались открыть, не существует',
                                    type='error')
        return redirect('/')


def get_bet_info(request, bet_id):
    delete_bets()
    bet = Bet.objects.get(id=bet_id)
    response = {
        'id': bet_id,
        'username': bet.player.user.username,
        'cost': bet.cost,
        'time': bet.time,
        'time_left': bet.time_left,
        'ongoing': bet.ongoing,
        'opponent': bet.opponent.user.username,
    }
    return JsonResponse(response)


@login_required(login_url='/startpage/')
def duel(request, bet_id):
    player = Player.objects.get(user__username=request.user.username)
    try:
        bet = Bet.objects.get(id=bet_id)
    except Bet.DoesNotExist:
        return redirect('/')
    if request.user.username != duel_rooms[bet_id].duel_player.player.user.username \
            and request.user.username != duel_rooms[bet_id].duel_opponent.player.user.username:
        return redirect('/')
    if bet.player.user.username != request.user.username and not bet.ongoing:
        opponent = Player.objects.get(user=request.user)
        if opponent.balance >= bet.cost:
            opponent.balance -= bet.cost
        else:
            Notification.objects.create(player=player, type='error',
                                        text='Ошибка - у вас недостаточно средств, чтобы принять эту ставку')
            return redirect('/')
    bet.acception_timestamp = round(timestamp.time())
    bet.save()
    comission = 1 if bet.acception_timestamp < player.no_comission_bonus_expiration_timestamp \
        or player.daily_offer_activated else 0.9
    profit = bet.cost * 2 * comission
    checked = None
    try:
        duel_room = duel_rooms[bet_id]
        checked = duel_room.results_correct()
    except KeyError:
        pass
    return render(request, 'bets/duel.html', {'bet': bet, 'profit': profit, 'checked': checked})


def get_battle_status(request):
    bet_id = request.GET.get('bet_id', None)
    response = None
    try:
        bet = Bet.objects.get(id=bet_id)
        if bet.ongoing:
            response = {'battle_status': 'ongoing'}
        elif bet.unfinished:
            response = {'battle_status': 'time_over'}
        else:
            response = {'battle_status': 'not_started'}
    except Bet.DoesNotExist:
        pass
    return JsonResponse(response)


@login_required(login_url='/startpage/')
def appeal(request, bet_id):
    player = Player.objects.get(user__username=request.user.username)
    try:
        bet = Bet.objects.get(id=bet_id)
    except Bet.DoesNotExist:
        Notification.objects.create(player=player, type='error', text='Ошибка - данной ставки не существует')
        return redirect('/')

    if request.method == 'GET':
        player = Player.objects.get(user__username=request.user.username)
        return render(request, 'bets/appeal.html', {'player': player})
    else:
        reason = request.POST.get('reason', None)
        text = request.POST.get('comment', None)
        new_appeal = Appeal.objects.create(bet=bet, reason=reason, text=text)
        files = request.FILES.getlist('files')
        for file in files:
            suffix = str(timestamp.time())
            temp_file = UploadedFile.objects.create(name=file.name + suffix, file=file)
            new_appeal.files.add(temp_file)
        new_appeal.save()
        return redirect('/')


def remove_notification(request, notification_id):
    response = dict()
    try:
        notification = Notification.objects.get(id=notification_id)
        notification.delete()
        response['status'] = 'OK'
    except Notification.DoesNotExist:
        response['status'] = 'ERROR'
    return JsonResponse(response)
